const books = [
  {
    title: 'Harry Potter and the Chamber of Secrets',
    author: 'J.K. Rowling',
  },
  {
    title: 'Jurassic Park',
    author: 'Michael Jackson',
  },
]

export const resolvers = {
  Query: {
    books: () => books,
    hello: () => 'wtf'
  },
}