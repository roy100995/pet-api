import dotEnv from 'dotenv'
import { ApolloServer } from 'apollo-server'
import { typeDefs } from './schema'
import { resolvers } from './resolvers'
import { connect } from './database/util'

dotEnv.config()

connect()

const PORT = process.env.PORT || 3000

const server = new ApolloServer({
  typeDefs,
  resolvers
})


server.listen(PORT).then(({ url }) => {
  console.log(`🚀  Server running at ${url}`)
})