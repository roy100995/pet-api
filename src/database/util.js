import mongoose from 'mongoose'

export function connect() {
  mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
    .then(db => console.log(`DB is connected to ${db.connection.host}`))
    .catch(e => console.error(e))
}